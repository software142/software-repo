from rest_framework.serializers import ModelSerializer
from medical.models import hospital, has_test, package


class HospitalListSerializer(ModelSerializer):
    class Meta:
        model = hospital
        fields = [
            'name',
            'type',
            'email',
            'phone',
            'division',
        ]


class HospitalDetailsSerializer(ModelSerializer):
    class Meta:
        model = hospital
        fields = [
            'id',
            'name',
            'type',
            'email',
            'phone',
            'address',
            'zip_code',
            'division',
        ]


class HospitalCreateSerializer(ModelSerializer):
    class Meta:
        model = hospital
        fields = [
            '__all__',
        ]


# Diagnostic test serializers
class TestListSerializer(ModelSerializer):
    class Meta:
        model = has_test
        fields = [
            'id',
            'name',
            'hospital',
            'test_details',
            'price',
            'delivary_in',
            'is_home_deliverable'
        ]


class TestCreateSerializer(ModelSerializer):
    class Meta:
        model = has_test
        fields = [
            'name',
            'hospital',
            'test_details',
            'price',
            'delivary_in',
            'is_home_deliverable'
        ]


class TestReadSerializer(ModelSerializer):
    class Meta:
        model = has_test
        fields = [
            'pk',
            'name',
            'hospital',
            'test_details',
            'price',
            'delivary_in',
            'is_home_deliverable'
        ]


# Package Serializer
class PackageListSerializer(ModelSerializer):
    class Meta:
        model = package
        fields = [
            'pk',
            'name',
            'hospital',
            'details',
            'price',
            'delivary_in',
            'remarks',
        ]


class PackageCreateSerializer(ModelSerializer):
    class Meta:
        model = package
        fields ='__all__'
