from django.urls import path
from medical.REST.views import (
    #Hospital API URLs Mapping
    HospitalListApi,
    HospitalCreateApi,
    HospitalRetrieveApi,
    HospitalUpdateApi,
    HospitalDeleteApi,

    #Test API URLs Mapping
    HasTestListAPI,
    HasTestCreateAPI,
    HasTestReadAPI,
    HasTestUpdateAPI,
    HasTestDeleteAPI,

    # Package API URLs Mapping
    PackageListApi,
    PackageCreateApi,
    PackageRetrieveApi,
    PackageUpdateAPI,
    PackageDeleteAPI
)
app_name = "medical"


urlpatterns = [
    path('api/hospitals/', HospitalListApi.as_view(), name="hospitals"),
    path('api/hospital/create', HospitalCreateApi.as_view(), name="create"),
    path('api/hospital/<int:id>', HospitalRetrieveApi.as_view(), name="hospital"),
    path('api/hospital/update/<int:id>', HospitalUpdateApi.as_view(), name="update"),
    path('api/hospital/delete/<int:id>', HospitalDeleteApi.as_view(), name="delete"),

    #Has_test API URLs
    path('api/tests', HasTestListAPI.as_view(), name="tests"),
    path('api/test/create', HasTestCreateAPI.as_view(), name="create_test"),
    path('api/test/<int:id>', HasTestReadAPI.as_view(), name="read_test"),
    path('api/test/update/<int:id>', HasTestUpdateAPI.as_view(), name="update_test"),
    path('api/test/delete/<int:id>', HasTestDeleteAPI.as_view(), name="delete_test"),


    # Packages API URLs
    path('api/packages', PackageListApi.as_view(), name="packages"),
    path('api/package/create', PackageCreateApi.as_view(), name="create_package"),
    path('api/package/<int:id>', PackageRetrieveApi.as_view(), name="read_package"),
    path('api/package/update/<int:id>', PackageUpdateAPI.as_view(), name="update_package"),
    path('api/package/delete/<int:id>', PackageDeleteAPI.as_view(), name="delete_package")
]
