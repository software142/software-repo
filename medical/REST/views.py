from django.db.models import Q
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView
)
from medical.models import hospital, has_test, package
from .serializers import (
    HospitalListSerializer,
    HospitalDetailsSerializer,
    HospitalCreateSerializer,

    # Has_test serializers
    TestListSerializer,
    TestReadSerializer,

    # Package serializers
    PackageListSerializer,
    PackageCreateSerializer
)


class HospitalListApi(ListAPIView):
    serializer_class = HospitalListSerializer

    def get_queryset(self, *args, **kwargs):
        query_set = hospital.objects.all()
        query = self.request.GET.get('q')
        if query:
            query_set = query_set.filter(
                Q(name__first_name__icontains=query) |
                Q(name__last_name__icontains=query) |
                Q(name__username__icontains=query) |
                Q(name__last_name__icontains=query) |
                Q(type__icontains=query) |
                Q(email__icontains=query) |
                Q(division__icontains=query)
            ).distinct()
        return query_set


class HospitalCreateApi(CreateAPIView):
    queryset = hospital.objects.all()
    serializer_class = HospitalCreateSerializer


class HospitalRetrieveApi(RetrieveAPIView):
    queryset = hospital.objects.all()
    serializer_class = HospitalDetailsSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class HospitalUpdateApi(UpdateAPIView):
    queryset = hospital.objects.all()
    serializer_class = HospitalListSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class HospitalDeleteApi(DestroyAPIView):
    queryset = hospital.objects.all()
    serializer_class = HospitalListSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


# Has_test API Views
class HasTestListAPI(ListAPIView):
    serializer_class = TestListSerializer

    def get_queryset(self, *args, **kwargs):
        query_set = has_test.objects.all()
        query = self.request.GET.get('q')
        if query:
            query_set = query_set.filter(
                Q(name__name__contains=query) |
                Q(name__category__name__icontains=query) |
                Q(test_details__icontains=query)
            ).distinct()
        return query_set


class HasTestCreateAPI(CreateAPIView):
    queryset = has_test.objects.all()
    serializer_class = TestListSerializer


class HasTestReadAPI(RetrieveAPIView):
    queryset = has_test.objects.all()
    serializer_class = TestReadSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class HasTestUpdateAPI(UpdateAPIView):
    queryset = has_test.objects.all()
    serializer_class = TestListSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class HasTestDeleteAPI(DestroyAPIView):
    queryset = has_test.objects.all()
    serializer_class = TestListSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


# package API Views
class PackageListApi(ListAPIView):
    serializer_class = PackageListSerializer

    def get_queryset(self, *args, **kwargs):
        query_set = package.objects.all()
        query = self.request.GET.get('q')
        if query:
            query_set = query_set.filter(
                Q(name__icontains=query) |
                Q(hospital__name__first_name__icontains=query) |
                Q(hospital__name__last_name__icontains=query) |
                Q(details__icontains=query) |
                Q(remarks__icontains=query)
            ).distinct()
        return query_set


class PackageCreateApi(CreateAPIView):
    queryset = package.objects.all()
    serializer_class = PackageCreateSerializer


class PackageRetrieveApi(RetrieveAPIView):
    queryset = package.objects.all()
    serializer_class = PackageListSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class PackageUpdateAPI(UpdateAPIView):
    queryset = package.objects.all()
    serializer_class = PackageListSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class PackageDeleteAPI(DestroyAPIView):
    queryset = package.objects.all()
    serializer_class = PackageListSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'