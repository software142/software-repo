from django.db.models import Q
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView
)
from accounts.models import patient, shipping
from .serializers import (
    PatientListSerializer,
    ShippingListSerilizer,
    ShippingDetailsSerializer
)


class PatientCreateApi(CreateAPIView):
    queryset = patient.objects.all()
    serializer_class = PatientListSerializer


class PatientListApi(ListAPIView):
    serializer_class = PatientListSerializer

    def get_queryset(self, *args, **kwargs):
        query_set = patient.objects.all()
        query = self.request.GET.get('q')
        if query:
            query_set = query_set.filter(
                Q(name__first_name__icontains=query) |
                Q(name__last_name__icontains=query) |
                Q(birth_date__icontains=query) |
                Q(weight__icontains=query) |
                Q(height__icontains=query) |
                Q(smoker__icontains=query)
            ).distinct()
        return query_set


class PatientRetrieveApi(RetrieveAPIView):
    queryset = patient.objects.all()
    serializer_class = PatientListSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class PatientUpdateApi(UpdateAPIView):
    queryset = patient.objects.all()
    serializer_class = PatientListSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class PatientDeleteApi(DestroyAPIView):
    queryset = patient.objects.all()
    serializer_class = PatientListSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class ShippingCreateApi(CreateAPIView):
    queryset = shipping.objects.all()
    serializer_class = ShippingDetailsSerializer


class ShippingListAPI(ListAPIView):
    serializer_class = ShippingListSerilizer

    def get_queryset(self, *args, **kwargs):
        query_set = shipping.objects.all()
        query = self.request.GET.get('q')
        if query:
            query_set = query_set.filter(
                Q(city__icontains=query) |
                Q(area__icontains=query) |
                Q(address__icontains=query) |
                Q(alternative_mobile_no__icontains=query)
            ).distinct()
        return query_set


class ShippingRetrieveApi(RetrieveAPIView):
    queryset = shipping.objects.all()
    serializer_class = ShippingDetailsSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class ShippingUpdateApi(UpdateAPIView):
    queryset = shipping.objects.all()
    serializer_class = ShippingDetailsSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class ShippingDeleteApi(DestroyAPIView):
    queryset = shipping.objects.all()
    serializer_class = ShippingDetailsSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'