from rest_framework.serializers import ModelSerializer
from accounts.models import patient, shipping


class PatientListSerializer(ModelSerializer):
    class Meta:
        model = patient
        fields = '__all__'


class ShippingListSerilizer(ModelSerializer):
    class Meta:
        model = shipping
        exclude = ['id']


class ShippingDetailsSerializer(ModelSerializer):
    class Meta:
        model = shipping
        fields = '__all__'
