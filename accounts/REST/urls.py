from django.urls import path
from accounts.REST.views import (
    PatientListApi,
    PatientCreateApi,
    PatientRetrieveApi,
    PatientUpdateApi,
    PatientDeleteApi,


    ShippingCreateApi,
    ShippingListAPI,
    ShippingRetrieveApi,
    ShippingUpdateApi,
    ShippingDeleteApi
)
app_name = "medical"


urlpatterns = [
    path('api/patient/create', PatientCreateApi.as_view(), name="patient_create"),
    path('api/patients/', PatientListApi.as_view(), name="patient"),
    path('api/patient/<int:id>', PatientRetrieveApi.as_view(), name="single_patient"),
    path('api/patient/update/<int:id>', PatientUpdateApi.as_view(), name="update_patient"),
    path('api/patient/delete/<int:id>', PatientDeleteApi.as_view(), name="delete_patient"),


    path('api/shipping', ShippingListAPI.as_view(), name="shipping"),
    path('api/shipping/create', ShippingCreateApi.as_view(), name="shipping_create"),
    path('api/shipping/<int:id>', ShippingRetrieveApi.as_view(), name="single_shipping"),
    path('api/shipping/update/<int:id>', ShippingUpdateApi.as_view(), name="update_shipping"),
    path('api/shipping/delete/<int:id>', ShippingDeleteApi.as_view(), name="delete_shipping"),


]
